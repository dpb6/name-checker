#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
@author: David, dpb6@duke.edu, david.bradway@gmail.com
"""
#import Pandas as pd
#names2014 = pd.read_csv('names/yob2014.txt',names=['name','sex','number'])
#names2014_indexed = names2014.set_index(['sex','name']).sort_values(by='number',ascending=False)
#topM = names2014_indexed.loc['M'].head(25)
#topF = names2014_indexed.loc['F'].head(25)

from wordcloud import WordCloud
import random
import Matplotlib.pyplot as pp

# Generate word cloud images
textM='Noah Liam Mason Jacob William Ethan Michael Alexander James Daniel Elijah Benjamin Logan Aiden Jayden Matthew Jackson David Lucas Joseph Anthony Andrew Samuel Gabriel Joshua'
textF='Emma Olivia Sophia Isabella Ava Mia Emily Abigail Madison Charlotte Harper Sofia Avery Elizabeth Amelia Evelyn Ella Chloe Victoria Aubrey Grace Zoey Natalie Addison Lillian'

wordcloudM = WordCloud(width=480,height=480,ranks_only=True,background_color='white').generate(textM)
wordcloudF = WordCloud(width=480,height=480,ranks_only=True,background_color='white').generate(textF)

# http://hslpicker.com/
# Pink hsl(320, 100%, 54%)
def pink_color_func(word, font_size, position, orientation, random_state=None, **kwargs):
return "hsl({}, {}%, {}%)".format(str(random.randint(280, 320)),str(random.randint(50, 100)),str(random.randint(20, 80)))

# Blue hsl(240, 100%, 55%)
def blue_color_func(word, font_size, position, orientation, random_state=None, **kwargs):
return "hsl({}, {}%, {}%)".format(str(random.randint(200, 250)),str(random.randint(50, 100)),str(random.randint(20, 80)))

# Display the generated image in matplotlib way:
pp.figure()
pp.imshow(wordcloudM.recolor(color_func=blue_color_func))
pp.axis("off")

pp.figure()
pp.imshow(wordcloudF.recolor(color_func=pink_color_func))
pp.axis("off")
